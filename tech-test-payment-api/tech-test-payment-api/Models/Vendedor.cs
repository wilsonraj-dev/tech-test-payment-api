﻿using System.ComponentModel.DataAnnotations;

namespace tech_test_payment_api.Models
{
    public class Vendedor
    {
        public int Id { get; set; }

        [Required]
        [StringLength(80)]
        public string Nome { get; set; }

        [StringLength(14)]
        public string Cpf { get; set; }

        [EmailAddress]
        public string Email { get; set; }

        [StringLength(20)]
        public string Telefone { get; set; }
    }
}
