﻿namespace tech_test_payment_api.Models
{
    public class Venda
    {
        public int Id { get; set; }

        public DateTime DataVenda { get; set; }

        public string Produtos { get; set; }

        public int VendedorId { get; set; }

        public Status Status { get; set; }
    }
}
