﻿using System.ComponentModel.DataAnnotations;
using System.ComponentModel;

namespace tech_test_payment_api.Models
{
    public enum Status
    {
        [Description("Aguardando Pagamento")]
        [Display(Name = "Aguardando Pagamento")]
        AguardandoPagamento = 0,

        [Description("Pagamento aprovado")]
        [Display(Name = "Pagamento aprovado")]
        PagamentoAprovado = 1,

        [Description("Enviado para transportadora")]
        [Display(Name = "Enviado para transportadora")]
        EnviadoTransportadora = 2,

        [Description("Entregue")]
        [Display(Name = "Entregue")]
        Entregue = 3,

        [Description("Cancelada")]
        [Display(Name = "Cancelada")]
        Cancelada = 4,
    }
}
