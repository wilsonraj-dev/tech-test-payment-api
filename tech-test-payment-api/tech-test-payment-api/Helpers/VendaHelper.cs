﻿using tech_test_payment_api.Models;

namespace tech_test_payment_api.Helpers
{
    public class VendaHelper
    {
        internal Status ValidarTransacoesStatus(Venda venda, Status status)
        {
            if ((venda.Status == Status.AguardandoPagamento && status == Status.PagamentoAprovado) ||
                (venda.Status == Status.AguardandoPagamento && status == Status.Cancelada))
                return status;
            else if ((venda.Status == Status.PagamentoAprovado && status == Status.EnviadoTransportadora) ||
                     (venda.Status == Status.PagamentoAprovado && status == Status.Cancelada))
                return status;
            else if (venda.Status == Status.EnviadoTransportadora && status == Status.Entregue)
                return status;
            else
                throw new Exception($"Não é possível transitar do Status {venda.Status} para o status {status}");
        }
    }
}
