﻿using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using tech_test_payment_api.Models;
using tech_test_payment_api.Service.Interface;

namespace tech_test_payment_api.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class VendedoresController : ControllerBase
    {
        private readonly IVendedorService _vendedorService;

        public VendedoresController(IVendedorService vendedorService)
        {
            _vendedorService = vendedorService;
        }

        [HttpPost]
        public async Task<ActionResult> CadastrarVendedor(Vendedor vendedor)
        {
            try
            {
                if (vendedor == null)
                    return NotFound("Nenhum dado encontrado");
                else
                {
                    await _vendedorService.CadastrarVendedor(vendedor);
                    return Ok(vendedor);
                }
            }
            catch
            {
                return BadRequest("Request inválido");
            }
        }
    }
}
