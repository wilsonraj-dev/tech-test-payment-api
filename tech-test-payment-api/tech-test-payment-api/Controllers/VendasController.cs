﻿using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using System.Runtime.CompilerServices;
using tech_test_payment_api.Context;
using tech_test_payment_api.Models;
using tech_test_payment_api.Service.Interface;

namespace tech_test_payment_api.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class VendasController : ControllerBase
    {
        private readonly IVendaService _vendaService;
        private readonly AppDbContext _context;

        public VendasController(IVendaService vendaService, AppDbContext context)
        {
            _vendaService = vendaService;
            _context = context;
        }

        [HttpPost]
        public async Task<ActionResult> CadastrarVenda(Venda venda)
        {
            try
            {
                if (venda == null)
                    return NotFound("Nenhum dado encontrado");
                else
                {
                    await _vendaService.CadastrarVenda(venda);
                    return Ok(venda);
                }
            }
            catch
            {
                return BadRequest("Request inválido");
            }
        }

        [HttpGet("{id:int}", Name = "ConsultarVenda")]
        public async Task<ActionResult> ConsultarVenda(int id)
        {
            try
            {
                var idVenda = await _vendaService.ConsultarVenda(id);
                if (idVenda == null)
                    return NotFound("Nenhuma venda encontrada");
                else
                    return Ok(idVenda);
            }
            catch
            {
                return BadRequest("Request inválido");
            }
        }

        [HttpPatch("{id:int}")]
        public async Task<ActionResult> AtualizarStatusVenda(int id, Status status)
        {
            try
            {
                await _vendaService.AtualizarStatusVenda(id, status);
                var vendaAtualizada = _context.Vendas.Find(id);
                if (vendaAtualizada == null)
                    return NotFound("Nenhuma venda encontrada");
                else
                    return Ok(vendaAtualizada);
            }
            catch (Exception e)
            {
                return BadRequest("Request inválido " + e);
            }
        }
    }
}
