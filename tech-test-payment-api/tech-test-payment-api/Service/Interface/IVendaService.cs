﻿using tech_test_payment_api.Models;

namespace tech_test_payment_api.Service.Interface
{
    public interface IVendaService
    {
        Task CadastrarVenda(Venda venda);
        Task<Venda> ConsultarVenda(int id);
        Task AtualizarStatusVenda(int id, Status status);
    }
}
