﻿using tech_test_payment_api.Models;

namespace tech_test_payment_api.Service.Interface
{
    public interface IVendedorService
    {
        Task CadastrarVendedor(Vendedor vendedor);
    }
}
