﻿using tech_test_payment_api.Context;
using tech_test_payment_api.Helpers;
using tech_test_payment_api.Models;
using tech_test_payment_api.Service.Interface;

namespace tech_test_payment_api.Service.Classe
{
    public class VendaService : IVendaService
    {
        private readonly AppDbContext _context;

        public VendaService(AppDbContext context)
        {
            _context = context;
        }

        public async Task CadastrarVenda(Venda venda)
        {
            if (venda == null)
                throw new ArgumentNullException("Nenhum dado encontrado");
            else
            {
                _context.Vendas.Add(venda);
                await _context.SaveChangesAsync();
            }
        }

        public async Task<Venda> ConsultarVenda(int id)
        {
            if (id == 0)
                throw new ArgumentNullException("Nenhum dado encontrado");
            else
            {
                var venda = await _context.Vendas.FindAsync(id);
                return venda;
            }
        }

        public async Task AtualizarStatusVenda(int id, Status status)
        {
            var venda = _context.Vendas.Find(id);
            if (venda == null)
                throw new ArgumentNullException("Nenhum dado encontrado");
            else
            {
                VendaHelper vendaHelper = new VendaHelper();
                venda.Status = vendaHelper.ValidarTransacoesStatus(venda, status);
                await _context.SaveChangesAsync();
            }
        }
    }
}
