﻿using tech_test_payment_api.Context;
using tech_test_payment_api.Models;
using tech_test_payment_api.Service.Interface;

namespace tech_test_payment_api.Service.Classe
{
    public class VendedorService : IVendedorService
    {
        private readonly AppDbContext _context;

        public VendedorService(AppDbContext context)
        {
            _context = context;
        }

        public async Task CadastrarVendedor(Vendedor vendedor)
        {
            if (vendedor == null)
                throw new ArgumentNullException("Nenhum dado encontrado");
            else
            {
                _context.Vendedores.Add(vendedor);
                await _context.SaveChangesAsync();
            }
        }
    }
}
